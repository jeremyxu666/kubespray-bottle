#### Local Git configurations

- Specify ssh key when clone: `GIT_SSH_COMMAND="ssh -i ~/.ssh/id_rsa" git clone git@gitlab.com:jeremyxu666/roads-to-cpp.git`
- Set git ssh key for local repo: `git config --local core.sshCommand "ssh -i ~/.ssh/id_rsa -F /dev/null"`
- Add kubespray as a git submodule: `git submodule add https://github.com/kubernetes-sigs/kubespray.git kubespray`
- Pull submodule: `git submodule update --init --recursive`
- Update submodule with remote: `git submodule update --remote --merge`, we can preform regular git operations in the 
`./kubespray` folder and after commit, go upper level to commit the submodule changes. The detailed changes will only
be seen in the submodule folder with `git log` but not in the gitlab web console.
- Simple create a virtual environment, and then do `pip3 install -r requirements.txt`


### Deploy cluster into AWS
- `tfenv list`: check what version is available and `tfenv use 1.1.7` to use a specific version needed
- `cd ./kubespray/contrib/terraform/aws && terraform plan --var-file="credentials.tfvars" -out tf.plan`
- `terraform apply tf.plan`

#### Prepare your environment first

```bash
python3 -m vevn .venv
source .venv/bin/activate
pip install -r requirements.txt # this will install the right ansible version, don't mess with the host python environment
```
```bash
#During this ssh session, we are going to use the right private key for bastion host to connect the other two nodes.
eval $(ssh-agent)
ssh-add -D
ssh-add ~/.ssh/private.key
```
- `ansible-playbook -i ./inventory/hosts ./cluster.yml -e ansible_user=admin -b --become-user=root --flush-cache`

##### Configure
- Connect to the master node:
    - `mkdir ~/.kube`
    - `sudo cp /etc/kubernetes/admin.config ~/.kube/config`
    - `sudo chown admin:admin ~/.kube/config`
- Connect to the node from localhost:
    - copy the config file on the ~/.kube/config
    - make sure to update the private ip to elb dns

### Deploy using vagrant
- Change the subnet range accordingly to the machine.
- Grant permission for vbox to run in the Systems Preference/Security&Privacy to allow
- `vagrant up --provision`: bring up the environment
- `vagrant ssh k8s-1`: to login into the node